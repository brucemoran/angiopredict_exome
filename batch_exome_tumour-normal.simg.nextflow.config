//container parameters; ensure these are OK for your system
//NB containers are defined per process below
//NBB bind to where your data is extant at least!
//NBBB set cacheDir to actual value that already exists
//NBBBB ensure enabled, autoMounts are available on your system

singularity {

  enabled = true
  autoMounts = true
  cacheDir = "/store2/singularity"
  runOptions = "--bind /store2/bmoran:/store2/bmoran"

}

params {

  //NB that task.memory causes some tools to fail, so using these predefs here
  //system-specific, check availability of resources
  full_javamem = "-Xmx120g"
  half_javamem = "-Xmx60g"
  quarter_javamem = "-Xmx30g"
  eighth_javamem = "-Xmx15g"
  twentieth_javamem = "-Xmx5g"

  //for ordering output plots from somaticConsensusVariants
  includeOrder = null

}

process {

  //containers per process
  withName:bbduke { container = "shub://brucemoran/Singularity:bbduk.centos7" }
  withName:fastp { container = "shub://brucemoran/Singularity:fastp.centos7" }
  withName:bwamem { container = "shub://brucemoran/Singularity:bwa.centos7" }
  withName:mrkdup { container = "shub://brucemoran/Singularity:picard-tools.centos7" }
  withName:gtkrcl { container = "shub://brucemoran/Singularity:gatk4.docker" }
  withName:mltmet { container = "shub://brucemoran/Singularity:picard-tools.centos7" }
  withName:fctcsv { container = "shub://brucemoran/Singularity:facets.centos7" }
  withName:msisen { container = "shub://brucemoran/Singularity:msisensor.centos7" }
  withName:mutct2 { container = "shub://brucemoran/Singularity:gatk4.docker" }
  withName:mutct2_contam { container = "shub://brucemoran/Singularity:gatk4.docker" }
  withName:mntstr { container = "shub://brucemoran/Singularity:manta-strelka2.docker" }
  withName:lancet { container = "shub://brucemoran/Singularity:lancet.centos7" }
  withName:vcfGRa { container = "shub://brucemoran/Singularity:granges_vcf.centos7-r_3.5.1_ens" }
  withName:mltiQC { container = "shub://brucemoran/Singularity:multiqc.centos7" }

  //VEP container hosted on github.com/brucemoran/Singularity/tools/annotation/
  //too fat to build on shub, hand-built
  withName:vepann { container = "${singularity.cacheDir}/vep-92.1.homo_sapiens_merged.grch37.centos7.simg" }

  //NB agnostic labels; see NextFlow docs for specifics to run on PBS, slurm etc.

  withLabel: c40_120G_cpu_mem {
      cpus = 40
      memory = 120.GB
  }
  withLabel: c20_60G_cpu_mem {
      cpus = 20
      memory = 60.GB
  }
  withLabel: c10_30G_cpu_mem {
      cpus = 10
      memory = 30.GB
  }
  withLabel: c8_24G_cpu_mem {
      cpus = 8
      memory = 24.GB
   }
  withLabel: c5_15G_cpu_mem {
      cpus = 5
      memory = 15.GB
  }
  withLabel: c2_6G_cpu_mem {
      cpus = 2
      memory = 6.GB
  }
}
