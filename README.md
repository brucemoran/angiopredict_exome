#ANGIOPREDICT Exome Somatic Variant Calling

###contact: bruce01campus@gmail.com

##Requirements
```
NextFlow version 19.01.0
Singularity
```

##DNAseq References 

###exomebed herein

###cosmicCGCtsv downloaded from COSMIC (cancer gene census)
```
mkdir DNAseq_references && cd DNAseq_references
nextflow run DNAseq_references.GRCh37.simg.nf \
  --exomebed 130717_HG19_Angio_SD_EZ.bed \
  --cosmicCGCtsv Census_allMon-Jan-22-10_41_08-2018.tsv \
  -with-timeline DNAseq_references.timeline.html \
  -with-report DNAseq_references.report.html \
  -c DNAseq_references.GRCh37.simg.nextflow.config

cd ../
```

##Run Variant Calling Analysis

###NB sample.csv supplied in Zenodo, recreate in this format based on paths to fastqs
```
nextflow run batch_exome_tumour-normal.simg.nf \
  -c batch_exome_tumour-normal.simg.nextflow.config \
  --sampleCsv data/sample.APD.csv \
  --refDir DNAseq_references/GRCh37
```