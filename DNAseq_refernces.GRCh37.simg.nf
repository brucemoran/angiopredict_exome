#!/usr/bin/env nextflow

params.help = ""

if (params.help) {
  log.info ''
  log.info '--------------------------------------------------------------------------------'
  log.info 'NEXTFLOW: MAKE REFERENCE-RELATED FILES AND INDICES FOR DNASEQ NEXTFLOW PIPELINES'
  log.info '--------------------------------------------------------------------------------'
  log.info ''
  log.info 'Usage: '
  log.info 'nextflow run DNAseq_references.simg.nf \
    --exomebed "/local/path/to/exome.bed" \
    --cosmicCGCtsv "/local/path/to/COSMIC/Census.tsv" \
    -with-timeline DNAseq_references.timeline.html \
    -with-report DNAseq_references.report.html'

  log.info ''
  log.info 'Mandatory arguments:'
  log.info '    --exomebed     STRING      exome bed file for intervals; download and supply path'
  log.info '    --cosmicCGCtsv  STRING    full path of COSMIC CGC tsv; download and supply path'
  log.info ''
  exit 1
}

/* 0.0: Global Variables
*/
params.refDir = "$baseDir/GRCh37"
params.ftpurl = "ftp.broadinstitute.org/bundle"

/* Exome bed channel
*/
Channel.fromPath("$params.exomebed", type: 'file').set{ bed_exome }

/* 0.1: Mutect2 Biallelic for GetPileupSummaries
* no Chr; start here as small files; max 25 users per GATK FTP; issues running previously!
*/
process biall {

  publishDir path: "$params.refDir", mode: "copy"

  output:
  file('mutect2_GetPileupSummaries*') into biallelicgz
  file('*gz.tbi') into nextdl

  """
  curl -u gsapubftp-anonymous:"" -o mutect2_GetPileupSummaries.vcf.gz ${params.ftpurl}/Mutect2/GetPileupSummaries/small_exac_common_3_b37.vcf.gz
  curl -u gsapubftp-anonymous:"" -o mutect2_GetPileupSummaries.vcf.gz.tbi ${params.ftpurl}/Mutect2/GetPileupSummaries/small_exac_common_3_b37.vcf.gz.tbi
  """
}
biallelicgz.subscribe { println "Completed: " + it.toString().tokenize("/").last() }

/* 1.0: Download GATK4 resource bundle (b37 only, sorry!) fasta
*/
process fasta_dl {

  publishDir path: "$params.refDir", mode: "copy"
  validExitStatus 0,1,2

  input:
  file(tbi) from nextdl

  output:
  file('*.fasta') into (fasta_bwa, fasta_seqza, fasta_msi, fasta_dict)
  file('*') into nextdll

  """
  ##http://lh3.github.io/2017/11/13/which-human-reference-genome-to-use
  curl -o human_g1k_v37.fasta.gz ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/technical/reference/human_g1k_v37.fasta.gz
  gzip -qdf human_g1k_v37.fasta.gz
  """
}

/* 1.05: Dictionary for fasta
*/
process dict_pr {

  publishDir path: "$params.refDir", mode: "copy"

  input:
  file(fa) from fasta_dict

  output:
  file('*.dict') into (dict_win, dict_exome)

  """
  DICTO=\$(echo $fa | sed 's/fasta/dict/')
  picard-tools CreateSequenceDictionary \
    R=$fa \
    O=\$DICTO
  """
}

/* 1.1: Download GATK4 resource bundle dbsnp, Mills
*/
process dbsnp_dl {

  input:
  file(fai) from nextdll

  output:
  file('*.vcf') into vcf_tabix

  """
  curl -u gsapubftp-anonymous:"" -o dbsnp_138.hg19.vcf.gz ${params.ftpurl}/hg19/dbsnp_138.hg19.vcf.gz

  gunzip -cd dbsnp_138.hg19.vcf.gz | sed 's/chr//g' > dbsnp_138.hg19.vcf
  """
}

/* 2.0: Fasta processing
*/
process fasta_pr {

  publishDir path: "$params.refDir", mode: "copy"

  input:
  file(fa) from fasta_bwa

  output:
  file('*') into complete_fasta

  """
  ##https://gatkforums.broadinstitute.org/gatk/discussion/2798/howto-prepare-a-reference-for-use-with-bwa-and-gatk
  samtools faidx $fa
  bwa index -a bwtsw $fa
  """
}
complete_fasta.subscribe { println "Completed: fasta index" }

/* 2.1: Dict processing
*/
process dict_pr2 {

  publishDir path: "$params.refDir", mode: "copy"

  input:
  file(win_dict) from dict_win

  output:
  file('*') into complete_dict

  """
  perl -ane 'if(\$F[0]=~m/SQ\$/){@sc=split(/:/,\$F[1]);@ss=split(/:/,\$F[2]); if(\$sc[1]!~m/[GLMT]/){ print "\$sc[1]\\t\$ss[1]\\n";}}' $win_dict > seq.dict.chr-size

  bedtools makewindows -g seq.dict.chr-size -w 35000000 | perl -ane 'if(\$F[1]==0){\$F[1]++;};print "\$F[0]:\$F[1]-\$F[2]\n";' > 35MB-window.bed
  """
}
complete_dict.subscribe { println "Completed: seqDictWindows" }

/* 2.0: Bed for exome
*/
process exome_bed {

  publishDir path: "$params.refDir", mode: "copy"

  input:
  file(dict) from dict_exome
  file(exome_bed) from bed_exome

  output:
  file('*') into allexome
  file("exome.bed") into exome_tabix

  when:
  params.exomebed

  """
  ##must test if all chr in fasta are in exome, else manta cries
  ##must test if all regions are greater than length zero or strelka cries
  perl -ane 'if(\$F[1] == \$F[2]){\$F[2]++;} print join("\\t", @F[0..\$#F]) . "\\n";' $exome_bed | sed 's/chr//g' > tmp.bed

  ##always make interval list so we are in line with fasta
  picard-tools BedToIntervalList I=tmp.bed O=exome.bed.interval_list SD=$dict

  ##BedToIntervalList (reason unknown) makes 1bp interval to 0bp interval, replace with original
  perl -ane 'if(\$F[0]=~m/^@/){print \$_;next;} if(\$F[1] == \$F[2]){\$f=\$F[1]; \$f--; \$F[1]=\$f; print join("\\t", @F[0..\$#F]) . "\\n";} else{print \$_;}' exome.bed.interval_list > exome.bed.interval_list1
  mv exome.bed.interval_list1 exome.bed.interval_list

  ##output BED
  grep -v "@" exome.bed.interval_list | cut -f 1,2,3,5 > exome.bed
  """
}
allexome.subscribe { println "Completed: " + it.toString().tokenize("/").last() }

/* 2.1: Tabix those requiring tabixing
*/
exome_tabix.mix(vcf_tabix).set { tabixn }
process tabix_files {

  publishDir path: "$params.refDir", mode: "copy"

  input:
  file(tbtbx) from tabixn

  output:
  file('*') into tabixd

  when:
  params.exomebed

  """
  ##tabix
  bgzip $tbtbx
  gunzip -cf $tbtbx".gz" > $tbtbx
  tabix $tbtbx".gz"
  """
}
tabixd.subscribe { println "Files tabix'd: " + it }

/* 3.0: Sequenza GC bias
*/
process seqnza {

  publishDir path: "$params.refDir", mode: "copy"

  input:
  file(fa) from fasta_seqza

  output:
  file('*') into sequenzaout

  """
  GENOMEGC50GZ=\$(echo $fa | sed -r 's/.fasta/.gc50Base.txt.gz/')
  sequenza−utils.py GC-windows −w 50 $fa | gzip > \$GENOMEGC50GZ
  """
}
sequenzaout.subscribe { println "Completed: " + it.toString().tokenize("/").last() }

/* 3.2: Process COSMIC Cancer Gene Census download
* no Chr
*/
process cosmic {

  publishDir "$params.refDir", mode: "copy"

  output:
  file('COSMIC_CGC.bed') into completedCGCbed

  when:
  params.cosmicCGCtsv

  """
  tail -n+2 ${params.cosmicCGCtsv} | perl -F"\\t" -ane 'if(\$F[3]!~m/:-\$/){
      @s=split(/[:-]/,\$F[3]);
      if((\$s[2]>0) && (\$s[0]!~m/\"/)){
        print "\$s[0]\\t\$s[1]\\t\$s[2]\\t\$F[0]\\n";}}' | \\
  sort -V > COSMIC_CGC.bed
  """
}
completedCGCbed.subscribe { println "Completed: " + it.toString().tokenize("/").last() }

/* 3.3: MSIsensor microsatellites
*/
process msisen {

  publishDir "$params.refDir", mode: "copy"

  input:
  file(fa) from fasta_msi

  output:
  file('*') into completedmsisensor

  script:
  """
  msisensor scan -d $fa -o msisensor_microsatellites.list
  """
}
completedmsisensor.subscribe { println "Completed: " + it.toString().tokenize("/").last() }
